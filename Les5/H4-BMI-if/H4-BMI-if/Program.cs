﻿using System;

namespace H4_BMI_ifElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef lengte in meter (vb: 1,8)");
            double lengte = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef gewicht in kg (vb: 85)");
            double gewicht = Convert.ToDouble(Console.ReadLine());

            double bmi = gewicht / (lengte * lengte);
            Console.WriteLine($"Je BMI: {bmi:0.00}");

            if (bmi<18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ondergewicht");
            }
            else if (bmi < 24.9)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Normaal");
            }
            else if (bmi < 29.9)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Overgewicht");
            }
            else if (bmi < 39.9)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Obese");
            }
            else //(bmi >= 39.9
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ernstig Obese");
            }
            Console.ResetColor();
        }
    }
}
