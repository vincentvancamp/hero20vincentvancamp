﻿using System;

namespace H4_BMI_Enum
{
    class Program
    {
        enum BmiWaarden
        {
            Ondergewicht,
            Normaal,
            Overgewicht,
            Obese,
            Ernstig_Obese //Kijk onderaan waarom dit nu niet ErnstigObese is
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Geef lengte in meter (vb: 1,8)");
            double lengte = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef gewicht in kg (vb: 85)");
            double gewicht = Convert.ToDouble(Console.ReadLine());

            double bmi = gewicht / (lengte * lengte);
            Console.WriteLine($"Je BMI: {bmi:0.00}");

            if (bmi < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(BmiWaarden.Ondergewicht);
            }
            else if (bmi < 24.9)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(BmiWaarden.Normaal);
            }
            else if (bmi < 29.9)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(BmiWaarden.Overgewicht);
            }
            else if (bmi < 39.9)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(BmiWaarden.Obese);
            }
            else //(bmi >= 39.9
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(BmiWaarden.Ernstig_Obese);
                /* ik heb hier een onderscore gezet omdat je geen spaties in een Enum mag zetten.
                 * normaal schrijf je de Enumwaarden PascalCase dus ErnstigObese,
                 * maar door een _ te gebruiken kan je ervoor zorgen dat je toch Ernstig Obese kan afprinten
                 * namelijk door volgende methode te gebruiken:
                 * Je zet de tekst om naar een string en roept dan Replace aan
                 * hierin zal je de tekst die als eerste parameter binnekomt vervangen met hetgeen in de tweede parameter
                */
                string outputObese = (BmiWaarden.Ernstig_Obese.ToString()).Replace("_", " ");
                Console.WriteLine(outputObese);
            }
            Console.ResetColor();
        }
    }
}
