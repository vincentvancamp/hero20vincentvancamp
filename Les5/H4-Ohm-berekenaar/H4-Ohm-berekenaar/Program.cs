﻿using System;

namespace H4_Ohm_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wil je berekenen? u, i, r");
            string keuze = Console.ReadLine();
            double u = 0;
            double r = 0;
            double i = 0;

            if (keuze == "u")
            {
                Console.WriteLine("Geef i");
                i = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Geef r");
                r = Convert.ToDouble(Console.ReadLine());

                u = r * i;
                Console.WriteLine($"u={u} V");
            }
            else if (keuze == "i")
            {
                Console.WriteLine("Geef u");
                u = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Geef r");
                r = Convert.ToDouble(Console.ReadLine());

                i =  u / r;
                Console.WriteLine($"i={i} A");
            }
            else if (keuze == "r")
            {
                Console.WriteLine("Geef u");
                u = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Geef i");
                i = Convert.ToDouble(Console.ReadLine());

                r = u / i;
                Console.WriteLine($"r={r} Ohm");
            }
            else
            {
                Console.WriteLine("Onbekende keuze");
            }
        }
    }
}
