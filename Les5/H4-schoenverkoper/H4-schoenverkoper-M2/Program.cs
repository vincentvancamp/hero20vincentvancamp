﻿using System;

namespace H4_schoenverkoper_M2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoeveel schoenen koopt de klant?");
            int aantal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Boven hoeveel schoenen wordt de korting gegeven?");
            int kortingboven = Convert.ToInt32(Console.ReadLine());
            int prijs = 0;

            if (aantal <= kortingboven)
            {
                prijs = aantal * 20; //je zou van 20 ook een constante van kunnen maken vb: const int NORMALEPRIJS = 20
            }
            else
            {
                prijs = kortingboven * 20;
                prijs += (aantal - kortingboven) * 10; //je zou van 10 ook een constante van kunnen maken vb: const int KORTINGPRIJS = 10
            }

            Console.WriteLine($"Prijs: {prijs}");
        }
    }
}
