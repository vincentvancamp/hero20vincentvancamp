﻿using System;

namespace H4_Orakeltje
{
    enum Sexes
    {
        Male,
        Female
    }
    class Program
    {
        static void Main(string[] args)
        {
            Sexes sex = Sexes.Female;
            Console.WriteLine("Wat is je geslacht?");
            string answer = Console.ReadLine();

            if (answer == "m")
            {
                sex = Sexes.Male;
            }
            else if (answer == "v")
            {
                sex = Sexes.Female;
            }

            Console.WriteLine("Hoe oud ben je?");
            int age = Convert.ToInt32(Console.ReadLine());
            int maxAge;

            switch (sex)
            {
                case Sexes.Male:
                    maxAge = 120;
                    break;
                case Sexes.Female:
                    maxAge = 150;
                    break;
                // moet er zijn van compiler
                default:
                    maxAge = 150;
                    break;
            }
            Random ranGen = new Random();
            int remaining = ranGen.Next(5, maxAge - age + 1);

            Console.WriteLine($"Je hebt nog {remaining}jaar te leven!")
}
    }
}
