﻿using System;

namespace H4_Kleurcodes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat is de kleur van de eerste ring?"); 
            string color1 = Console.ReadLine(); 
            int ring1; 
            switch (color1)
            {
                case "zwart": ring1 = 0; 
                    break;
                case "bruin": ring1 = 1; 
                    break;
                case "rood": ring1 = 2; 
                    break;
                case "oranje": ring1 = 3; 
                    break;
                case "geel": ring1 = 4; 
                    break;
                case "groen": ring1 = 5; 
                    break;
                case "blauw": ring1 = 6; 
                    break;
                case "paars": ring1 = 7; 
                    break;
                case "grijs": ring1 = 8; 
                    break;
                case "wit": ring1 = 9; 
                    break;
                default:// later leer je een foutmelding geven
                    ring1 = 0;
                    break;
            }
            Console.WriteLine("Wat is de kleur van de tweede ring?");
            string color2 = Console.ReadLine();
            int ring2;
            switch(color2)
            {
                case"zwart":ring2 = 0;
                    break;
                case "bruin": ring2 = 1; 
                    break;
                case "rood": ring2 = 2; 
                    break;
                case "oranje": ring2 = 3; 
                    break;
                case "geel": ring2 = 4; 
                    break;
                case "groen": ring2 = 5; 
                    break;
                case "blauw": ring2 = 6; 
                    break;
                case "paars": ring2 = 7; 
                    break;
                case "grijs": ring2 = 8; 
                    break;
                case "wit": ring2 = 9; 
                    break;
                default:// later leer je een foutmelding geven
                    ring2 = 0;
                    break;
            }
            Console.WriteLine("Wat is de kleur van de derde ring?");
            string color3 = Console.ReadLine();
            int ring3;
            switch(color3)
            {
                case "grijs":ring3 = -2;
                    break;
                case"lichtbruin":ring3 = -1;
                    break;
                case"zwart":ring3 = 0;
                    break;
                case"bruin":ring3 = 1;
                    break;
                case"rood":ring3 = 2;
                    break;
                case"oranje":ring3 = 3;
                    break;
                case"geel":ring3 = 4;
                    break;
                case"groen":ring3 = 5;
                    break;
                case "blauw": ring3 = 6; 
                    break;
                case "paars": ring3 = 7; 
                    break;
                default:
                    // later leer je een foutmelding geven
                    ring3 = 0;
                    break;
            }
            // heel lange regels vermijden
            Console.Write($"Resultaat is {(10 * ring1 + ring2) * Math.Pow(10, ring3)}Ohm");
            Console.WriteLine($", ofwel {10 * ring1 + ring2}x{Math.Pow(10, ring3)}");
            }
    }
}
