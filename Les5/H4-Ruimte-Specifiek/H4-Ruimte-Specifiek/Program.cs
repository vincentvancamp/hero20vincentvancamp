﻿using System;

namespace H4_Ruimte_Specifiek
{
    class Program
    {
        enum Planets
        {
            Mercurius = 1,
            Venus,
            Aarde,
            Mars,
            Jupiter,
            Saturnus,
            Uranus,
            Neptunus,
            Pluto
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hoeveel weeg je?");
            double earthWeight = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Voor welke planeet wil je je gewicht kennen?");
            Console.WriteLine($"{(int)Planets.Mercurius}. {Planets.Mercurius}");
            Console.WriteLine($"{(int)Planets.Venus}. {Planets.Venus}");
            Console.WriteLine($"{(int)Planets.Aarde}. {Planets.Aarde}");
            Console.WriteLine($"{(int)Planets.Mars}. {Planets.Mars}");
            Console.WriteLine($"{(int)Planets.Jupiter}. {Planets.Jupiter}");
            Console.WriteLine($"{(int)Planets.Saturnus}. {Planets.Saturnus}");
            Console.WriteLine($"{(int)Planets.Uranus}. {Planets.Uranus}");
            Console.WriteLine($"{(int)Planets.Neptunus}. {Planets.Neptunus}");
            Console.WriteLine($"{(int)Planets.Pluto}. {Planets.Pluto}");

            Planets planet = (Planets)Convert.ToInt32(Console.ReadLine());
            // moet sowieso een waarde krijgen
            double multiplier = 1.0;

            switch (planet)
            {
                case Planets.Mercurius:
                    multiplier = 0.38;
                    break;
                case Planets.Venus:
                    multiplier = 0.91;
                    break;
                case Planets.Aarde:
                    multiplier = 1.0;
                    break;
                case Planets.Mars:
                    multiplier = 0.38;
                    break;
                case Planets.Jupiter:
                    multiplier = 2.34;
                    break;
                case Planets.Saturnus:
                    multiplier = 1.06;
                    break;
                case Planets.Uranus:
                    multiplier = 0.92;
                    break;
                case Planets.Neptunus:
                    multiplier = 1.19;
                    break;
                case Planets.Pluto:
                    multiplier = 0.06;
                    break;
            }
            Console.WriteLine($"Daar weeg je {earthWeight * multiplier:F2}kg.");
        }
    }
}
